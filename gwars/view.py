#!/usr/bin/env python

import curses

class View():

    def __init__(self):
        self.system = None
        self.width = 80
        self.height = 24
        self.top = curses.initscr()
        self.top.border(1)
        self.map_win = curses.newwin(2, 80, 0, 0)
        self.info_win = curses.newwin(21, 80, 1, 0)

    def run(self):
        curses.start_color()
        curses.init_pair(1, curses.COLOR_WHITE, 
            curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_GREEN,
            curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_RED,
            curses.COLOR_BLACK)
        WHITE = curses.color_pair(1)
        GREEN = curses.color_pair(2)
        RED = curses.color_pair(3)
        curses.curs_set(0)
        while True:
            self.map_win.erase()
            self.map_win.border(1)
            self.map_win.attrset(WHITE)
            self.map_win.addch(0, 1, ord('o'), curses.A_NORMAL)
            self.map_win.addch(0, 4, ord('o'), curses.A_NORMAL)
            self.map_win.attrset(GREEN)
            self.map_win.addch(0, 20, ord('x'), curses.A_BOLD)
            self.info_win.erase()
            self.info_win.border(1)
            self.info_win.addstr(1, 1, "ddcddcs")
            self.map_win.overlay(self.top)
            self.info_win.overlay(self.top)
            self.map_win.refresh()
            self.info_win.refresh()
            #self.info_win.getstr(1, 0)
            char = self.map_win.getch()
            if char == ord('q'):
                break
        curses.endwin()
