import os
from setuptools import setup

# Starfinder
# Find observable objects in your night sky.

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "gwars",
    version = "0.0.1",
    description = "galaxy wars",
    author = "Johan Nestaas",
    author_email = "johannestaas@gmail.com",
    license = "GPLv3+",
    keywords = "galaxy entertainment games strategy tbs 4x",
    url = "https://bitbucket.org/johannestaas/gwars",
    packages=['gwars'],
    package_dir={'gwars': 'gwars'},
    long_description=read('README.md'),
    classifiers=[
        #'Development Status :: 1 - Planning',
        #'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',
        #'Development Status :: 6 - Mature',
        #'Development Status :: 7 - Inactive',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Intended Audience :: End Users/Desktop',
        'Environment :: Console',
        'Environment :: X11 Applications :: Qt',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Topic :: Utilities',
    ],
    install_requires=[
    ],
    entry_points = {
        'console_scripts': [
            'gwars = gwars.bin:gwars',
        ],
    },
    #package_data = {
        #'gwars': ['catalog/*.edb'],
    #},
    #include_package_data = True,
)
